const express = require("express") //import express
const mongoose = require("mongoose") //import mongoose
const cors = require("cors") //import cors
const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")

//database connection
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"))
mongoose.connect('mongodb+srv://jino:jino123@cluster1.7j4kj.mongodb.net/course_booking?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//Local connection string:mongodb://localhost:27017/course_booking

//server setup
const app = express()
const port = 3000

//bodyparser middleware
app.use(express.json()) //only looks at requests where the Content-Type header is JSON
app.use(express.urlencoded({extended: true})) //allows POST request to include nested objects

//configure cors
app.use(cors())

//add all the routes
app.use("/api/users", userRoutes)
app.use("/api/courses", courseRoutes)

//server listening
app.listen(port, () => console.log(`Listening to port ${port}`))