//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can access specific parts of the query string
let params = new URLSearchParams(window.location.search)

//get returns the value of the key passed as an argument, then we store it in a variable
let courseId = params.get('courseId')

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`http:localhost:3000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)
})