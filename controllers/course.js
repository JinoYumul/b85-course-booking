const Course = require("../models/Course")

//Create a new course
module.exports.add = (params) => {
	let newCourse = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return newCourse.save().then((course, err) => {
		return (err) ? false : true //if there's an error, return false. Otherwise, return true
	})
}

//View all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(resultsFromFind => resultsFromFind)
}

//Get a specific course
module.exports.get = (params) => {
	console.log(params)
	return Course.findById(params.courseId).then(resultFromFindById => resultFromFindById)
}

module.exports.update = (params) => {
	let updatedCourse = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course, err) => {
		return (err) ? false : true
	})
}

//Archive (delete) a course
module.exports.archive = (params) => {
	let updateActive = {
		isActive: false
	}

	return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
		return (err) ? false : true
	})
}